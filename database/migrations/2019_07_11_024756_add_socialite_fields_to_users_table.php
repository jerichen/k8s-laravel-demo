<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Traits\SchemaTrait;

class AddSocialiteFieldsToUsersTable extends Migration
{
    use SchemaTrait;

    protected $connection = 'mysql';
    protected $table_name = 'users';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schemaTable(function (Blueprint $table) {
            if (Schema::connection($this->getConnection())
                ->hasTable($this->getTableName())) {
                $table->tinyInteger('login_type')->after('id');
                $table->string('provider_id')->nullable()->after('login_type');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schemaTable(function (Blueprint $table) {
            if (Schema::connection($this->getConnection())
                ->hasColumn($this->getTableName(), 'login_type')) {
                $table->dropColumn('login_type');
            }

            if (Schema::connection($this->getConnection())
                ->hasColumn($this->getTableName(), 'login_type')) {
                $table->dropColumn('provider_id');
            }
        });
    }
}
