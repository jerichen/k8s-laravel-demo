<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Traits\SchemaTrait;

class CreateUsersTable extends Migration
{
    use SchemaTrait;

    protected $connection = 'mysql';
    protected $table_name = 'users';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schemaCreate(function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->tinyInteger('status');
            $table->string('api_token', 60)->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schemaDrop(function(Blueprint $table, $table_name) {
            $table->dropIndex("{$table_name}_email_unique");
            $table->dropIndex("{$table_name}_api_token_unique");
        });
    }
}
