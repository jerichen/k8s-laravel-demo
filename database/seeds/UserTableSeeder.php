<?php

use App\Models\Entities\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'jerichen',
            'email' => 'jeri.chen0110@gmail.com',
            'status' => User::STATUS_OPEN,
            'login_type' => User::LOGIN_TYPE_GOOGLE,
        ]);

        factory(User::class)->create([
            'name' => 'jerichen',
            'email' => 'jeri.chen0805@yahoo.com.tw',
            'status' => User::STATUS_OPEN,
            'login_type' => User::LOGIN_TYPE_FACEBOOK,
        ]);
    }
}
