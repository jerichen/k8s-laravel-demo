<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

# google login
Route::get('login/google', 'Auth\GoogleLoginController@redirectToProvider')
    ->name('login.google');
Route::get('login/google/callback', 'Auth\GoogleLoginController@handleProviderCallback')
    ->name('login.google.callback');

# facebook login
Route::get('login/facebook', 'Auth\FacebookLoginController@redirectToProvider')
    ->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\FacebookLoginController@handleProviderCallback')
    ->name('login.facebook.callback');

# line login
Route::get('login/line', 'Auth\LineLoginController@redirectToProvider')
    ->name('login.line');
Route::get('login/line/callback', 'Auth\LineLoginController@handleProviderCallback')
    ->name('login.line.callback');
