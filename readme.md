## Laravel Deploy Local
[![pipeline status](https://gitlab.com/jerichen/k8s-laravel-demo/badges/master/pipeline.svg)](https://gitlab.com/jerichen/k8s-laravel-demo/commits/master)
[![coverage report](https://gitlab.com/jerichen/k8s-laravel-demo/badges/local/coverage.svg)](https://gitlab.com/jerichen/k8s-laravel-demo/commits/local)

### 專案內容
- Laravel Project
    1. 簡單建置 Social-Login (Facebook、Google、Line)
    2. Social-Login phpunit test
- Dockerfile files .
- Kubernetes yaml files .
- gitlab-ci.yml file .

### Local Demo Quick Start
- $ kubectl apply -f kubernetes/env-configmap.yaml
- $ kubectl apply -f kubernetes/pv.yaml
- $ kubectl apply -f kubernetes/pvc.yaml
- $ kubectl apply -f kubernetes/k8s-laravel-demo-deploy.yaml
- $ kubectl apply -f kubernetes/k8s-laravel-demo-service.yaml

### Dockerfile deploy Local Quick Start
- $ docker build -f ./docker/Dockerfile.nginx --tag nginx .
- $ docker build -f ./docker/Dockerfile.php-fpm --tag php-fpm .

### Kubectl Login bash
- $ kubectl exec -it {pod-name} -- /bin/sh

