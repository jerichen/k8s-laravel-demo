<?php

namespace App\Repositories;

use App\Models\Entities\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository
{
    protected $new_local_user;
    protected $login_type;

    protected function setLoginType($type)
    {
        $this->login_type = $type;
    }

    protected function getLocalUser($provider_id)
    {
        $local_user = User::where('provider_id', $provider_id)
            ->where('login_type', $this->login_type)
            ->first();

        return $local_user;
    }

    public function loginWithGoogle($user)
    {
        $this->setLoginType(User::LOGIN_TYPE_GOOGLE);
        $local_user = $this->getLocalUser($user->id);

        if (!$local_user) {
            $local_user = $this->buildUserWithGoogle($user);
        }

        Auth::guard('web')->login($local_user);
        return $local_user;
    }

    public function loginWithFacebook($user)
    {
        $this->setLoginType(User::LOGIN_TYPE_FACEBOOK);
        $local_user = $this->getLocalUser($user->id);

        if (!$local_user) {
            $local_user = $this->buildUserWithFacebook($user);
        }

        Auth::guard('web')->login($local_user);
        return $local_user;
    }

    public function loginWithLine($user)
    {
        $this->setLoginType(User::LOGIN_TYPE_LINE);
        $local_user = $this->getLocalUser($user->id);

        if (!$local_user) {
            $local_user = $this->buildUserWithLine($user);
        }

        Auth::guard('web')->login($local_user);
        return $local_user;
    }

    protected function buildUserWithGoogle($user)
    {
        $local_user = new User();
        $local_user->name = $user->name;
        $local_user->email = $user->email;
        $local_user->password = Hash::make(substr($user->id, 0, 9) . Str::random(4));
        $local_user->status = User::STATUS_OPEN;
        $local_user->login_type = User::LOGIN_TYPE_GOOGLE;
        $local_user->provider_id = $user->id;
        $local_user->save();

        return $local_user;
    }

    protected function buildUserWithFacebook($user)
    {
        $local_user = new User();
        $local_user->name = $user->name;
        $local_user->email = $user->email;
        $local_user->password = Hash::make(substr($user->id, 0, 9) . Str::random(4));
        $local_user->status = User::STATUS_OPEN;
        $local_user->login_type = User::LOGIN_TYPE_FACEBOOK;
        $local_user->provider_id = $user->id;
        $local_user->save();

        return $local_user;
    }

    protected function buildUserWithLine($user)
    {
        $local_user = new User();
        $local_user->name = $user->name;
        $local_user->email = $user->id . '@line';
        $local_user->password = Hash::make(substr($user->id, 0, 9) . Str::random(4));
        $local_user->status = User::STATUS_OPEN;
        $local_user->login_type = User::LOGIN_TYPE_LINE;
        $local_user->provider_id = $user->id;
        $local_user->save();

        return $local_user;
    }
}
