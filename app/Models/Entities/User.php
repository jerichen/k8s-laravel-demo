<?php

namespace App\Models\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const STATUS_OPEN = 1;
    const STATUS_CLOSE = 2;
    const LOGIN_TYPE_EMAIL = 1;
    const LOGIN_TYPE_GOOGLE = 2;
    const LOGIN_TYPE_FACEBOOK = 3;
    const LOGIN_TYPE_LINE = 4;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'login_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'api_token',
        'provider_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function statusAry()
    {
        return [
            self::STATUS_OPEN => '啟用',
            self::STATUS_CLOSE => '非啟用',
        ];
    }

    public static function loginTypeAry()
    {
        return [
            self::EMAIL => 'email',
            self::GOOGLE => 'google',
            self::FACEBOOK => 'facebook',
            self::LINE => 'line',

        ];
    }
}
