<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Services\SocialService;

class FacebookLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $social_service;

    protected $fields = ['name', 'email', 'gender', 'verified', 'link', 'token_for_business'];
    protected $scopes = ['email'];

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SocialService $social_service)
    {
        $this->middleware('guest')->except('logout');

        $this->social_service = $social_service;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }

    public function redirectToProvider()
    {
        config(['services.facebook.redirect' => env('APP_URL') . env('FACEBOOK_REDIRECT')]);

        return Socialite::driver('facebook')
            ->fields($this->fields)
            ->scopes($this->scopes)
            ->redirect();

    }

    public function handleProviderCallback(Request $request)
    {
        config(['services.facebook.redirect' => env('APP_URL') . env('FACEBOOK_REDIRECT')]);
        $user = Socialite::driver('facebook')->user();

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user = $this->social_service->loginWithFacebook($user);

        if ($user) {
            return $this->sendLoginResponse($request);
        }
        return $this->sendFailedLoginResponse($request);
    }
}
