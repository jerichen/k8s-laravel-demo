<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use App\Services\SocialService;

class LineLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $social_service;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SocialService $social_service)
    {
        $this->middleware('guest')->except('logout');

        $this->social_service = $social_service;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }

    public function redirectToProvider()
    {
        config(['services.line.redirect' => env('APP_URL') . env('LINE_REDIRECT')]);

        return Socialite::driver('line')->redirect();
    }

    public function handleProviderCallback(Request $request)
    {
        config(['services.line.redirect' => env('APP_URL') . env('LINE_REDIRECT')]);
        $user = Socialite::driver('line')->user();

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user = $this->social_service->loginWithLine($user);

        if ($user) {
            return $this->sendLoginResponse($request);
        }
        return $this->sendFailedLoginResponse($request);
    }
}
