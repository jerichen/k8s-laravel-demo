<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Services\SocialService;

class GoogleLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $social_service;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SocialService $social_service)
    {
        $this->middleware('guest')->except('logout');

        $this->social_service = $social_service;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }

    public function redirectToProvider()
    {
        config(['services.google.redirect' => env('APP_URL') . env('GOOGLE_REDIRECT')]);

        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback(Request $request)
    {
        config(['services.google.redirect' => env('APP_URL') . env('GOOGLE_REDIRECT')]);
        $user = Socialite::driver('google')->user();

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $this->social_service->loginWithGoogle($user);

        if ($user) {
            return $this->sendLoginResponse($request);
        }
        return $this->sendFailedLoginResponse($request);
    }
}
