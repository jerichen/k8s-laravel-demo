<?php

namespace App\Services;

use App\Repositories\UserRepository;

class SocialService
{
    protected $user_repo;

    public function __construct(UserRepository $user_repo)
    {
        $this->user_repo = $user_repo;
    }

    public function loginWithGoogle($user)
    {
        return $this->user_repo->loginWithGoogle($user);
    }

    public function loginWithFacebook($user)
    {
        return $this->user_repo->loginWithFacebook($user);
    }

    public function loginWithLine($user)
    {
        return $this->user_repo->loginWithLine($user);
    }
}
