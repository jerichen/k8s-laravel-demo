@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-2">
                            <a href="{{route('login.google')}}" class="btn btn-secondary">
                                使用 Google 帳號登入
                            </a>

                            <a href="{{route('login.facebook')}}" class="btn btn-primary">
                                使用 Facebook 帳號登入
                            </a>

                            <a href="{{route('login.line')}}" class="btn btn-dark">
                                使用 Line 帳號登入
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
